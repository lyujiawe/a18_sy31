Node : robot_motion
Permet au robot d'avancer ou de reculer d'une distance donnée ou de tourner d'un angle donné.

Entrées :
    Topic par default :    /pose_odom (à modifier si besoin)
    Type de message :    PoseStamped
    Permet d'obtenir la pose du robot utilisée pour la régulation.

    Topic par default :    /move_forward
    Type de message :    Float32
    Consigne de distance (à envoyer une seule fois),
    le robot avancera si la consigne est positive et reculera sinon.

    Topic par default :    /turn_left
    Type de message :    Float32
    Consigne d'angle (à envoyer une seule fois),
    le robot tournera à droite si la consigne est positive et à gauche sinon.

Sorties :
    Topic par default :    /cmd_vel
    Type de message :    Twist
    Consigne de vitesse envoyée aux servomoteurs du robot.

    Topic par default :    /move_forward_finished
    Type de message :    Bool
    Booléen passant à False lorsque la consigne de distance est reçu et
    à True lorsque le roboot est arrivé à destination.

    Topic par default :    /move_forward_finished
    Type de message :    Bool
    Booléen passant à False lorsque la consigne d'angle est reçu et
    à True lorsque le roboot est arrivé à destination.

Paramètres :
    self.K_p = np.array([1, 1])
    Coefficients du régulateur proportionnel en distance et en angle.

    self.max_speed = np.array([0.05, 0.5])
    Vitesse linéaire et angulaire maximales envoyées au robot.

    self.accuracy = np.array([0.01, 0.05])
    Seuils en distance et en angle permettant de supposer que le robot à bien terminer son action.
    

