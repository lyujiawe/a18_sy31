#!/usr/bin/env python
import rospy
from geometry_msgs.msg import PoseStamped, Twist
from std_msgs.msg import Float32, Bool
from turtlesim.msg import Pose
import numpy as np
import time
import math

from tf.transformations import euler_from_quaternion

class To_move:
    def __init__(self):
        rospy.init_node('project', anonymous=True)
        self.move_forward_publisher = rospy.Publisher('/move_forward',Float32, queue_size=10)
        self.turn_left_publisher = rospy.Publisher('/turn_left',Float32, queue_size=10)
        self.move_forward_subscriber = rospy.Subscriber('/move_forward_finished',Bool,self.callback_sub)
        self.turn_left_subscriber = rospy.Subscriber('/turn_left_finished',Bool,self.callback_turn)
        self.next_action =True
    def callback_sub(self, msg):
        #self.move_forward_publisher.publish(Float32(0.5))
        self.next_action = True
        print("move forword finished!")
        
    def move2goal(self):
        goal_pose = Pose()
        goal_pose.x = input("entrez x ")
        goal_pose.y = input("entrez y ")
        distance = math.sqrt(goal_pose.x*goal_pose.x + goal_pose.y*goal_pose.y)
        angle = math.atan(goal_pose.x/goal_pose.y)
        while not rospy.is_shutdown():
            time.sleep(1.0)
            self.turn_left_publisher.publish(angle)
            time.sleep(2.0)
            self.move_forward_publisher.publish(distance)
        

    def callback_turn(self, msg):
        
        #self.turn_left_publisher.publish(Float32(3))
        self.next_action = True
        print("turn left finished!")

   

if __name__ == '__main__':
    try:
        x = To_move()
        time.sleep(1.0)
        x.move_forward_publisher.publish(Float32(0.5))
        x.move2goal()
    except rospy.ROSInterruptException:
        pass
