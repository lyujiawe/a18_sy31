#!/usr/bin/env python
import rospy
from geometry_msgs.msg import PoseStamped, Twist
from std_msgs.msg import Float32, Bool
import numpy as np

from tf.transformations import euler_from_quaternion

class RobotMotion:
    def __init__(self):
        # Creates a node with name 'imu2pose' and make sure it is a
        # unique node (using anonymous=True).
        rospy.init_node('robot_motion', anonymous=True)

        # Publisher which will publish to the topic '/cmd_vel'.
        self.velocity_publisher = rospy.Publisher('/cmd_vel', Twist, queue_size=10)

        # Publisher which will publish to the topic '/move_forward_finished'.
        self.move_forward_finished_publisher = rospy.Publisher('/move_forward_finished', Bool, queue_size=10)

        # Publisher which will publish to the topic '/turn_left_finished'.
        self.turn_left_finished_publisher = rospy.Publisher('/turn_left_finished', Bool, queue_size=10)


        # A subscriber to the topic '/pose_odom'. self.update_pose is called
        # when a message of type PoseStamped is received.
        self.pose_subscriber = rospy.Subscriber('/pose_odom', PoseStamped, self.update_velocity)

        # A subscriber to the topic '/move_forward'. self.move_forward is called
        # when a message of type Float32 is received.
        self.move_forward_subscriber = rospy.Subscriber('/move_forward', Float32, self.move_forward)
 
        # A subscriber to the topic '/turn_left'. self.turn_left is called
        # when a message of type Float32 is received.
        self.turn_left_subscriber = rospy.Subscriber('/turn_left', Float32, self.turn_left)

        self.pose = PoseStamped()
        self.pose2d = np.array([0.0, 0.0, 0.0])
        self.goal2d = np.array([0.0, 0.0, 0.0])
        self.cmd_vel = Twist()

        self.move_forward_flag = False
        self.turn_left_flag = False
        self.pose_observed = False
        #self.next_action = False

        # Parameters
        self.K_p = np.array([1, 1])
        self.max_speed = np.array([0.05, 0.5])
        self.accuracy = np.array([0.01, 0.05])

        # If we press control + C, the node will stop.
        rospy.spin()

    def move_forward(self, data):
        # Move forward if data.data > 0 and backward if data.data < 0
        if self.pose_observed:
            self.goal2d[0:2] = self.pose2d[0:2] + np.array([data.data*np.cos(self.pose2d[2]), data.data*np.sin(self.pose2d[2])])
            self.direction = np.array([np.cos(self.pose2d[2]), np.sin(self.pose2d[2])])
            self.move_forward_flag = True
            self.turn_left_flag = False
            self.cmd_vel = Twist()
            self.move_forward_finished_publisher.publish(Bool(False))
	
    def turn_left(self, data):
        # Turn left if data.data > 0 and right if data.data < 0
        if self.pose_observed:
            self.goal2d[0:2] = self.pose2d[0:2]
            self.goal2d[2] = self.pose2d[2] + data.data
            self.move_forward_flag = False
            self.turn_left_flag = True
            self.cmd_vel = Twist()
            self.turn_left_finished_publisher.publish(Bool(False))

    def update_velocity(self, data):
        # Read the new pose and update the velocity command
        self.pose2d = np.array([data.pose.position.x,
                                data.pose.position.y,
                                euler_from_quaternion([data.pose.orientation.x, 
                                                        data.pose.orientation.y, 
                                                        data.pose.orientation.z, 
                                                        data.pose.orientation.w])[2]])
        self.pose_observed = True

        if self.turn_left_flag:
            angle_error = self.goal2d[2] - self.pose2d[2]
            if np.abs(angle_error) < self.accuracy[1]:
                self.turn_left_flag = False
                self.cmd_vel.angular.z = 0.
                print 'turn left finished!'
                self.turn_left_finished_publisher.publish(Bool(True))
            else:
                self.cmd_vel.angular.z = self.K_p[1] * angle_error
                if np.abs(self.cmd_vel.angular.z) > self.max_speed[1]:
                    self.cmd_vel.angular.z = np.sign(self.cmd_vel.angular.z)*self.max_speed[1]
        elif self.move_forward_flag:
            distance_error = np.dot((self.goal2d[0:2] - self.pose2d[0:2]), self.direction)
            if np.abs(distance_error) < self.accuracy[0]:
                self.move_forward_flag = False
                self.cmd_vel.linear.x = 0.
                print 'move forward finished!'
                self.move_forward_finished_publisher.publish(Bool(True))
            else:
                self.cmd_vel.linear.x = self.K_p[0] * distance_error
                if np.abs(self.cmd_vel.linear.x) > self.max_speed[0]:
                    self.cmd_vel.linear.x = np.sign(self.cmd_vel.linear.x)*self.max_speed[0]

        self.velocity_publisher.publish(self.cmd_vel)


if __name__ == '__main__':
    try:
        RobotMotion()
    except rospy.ROSInterruptException:
        pass
